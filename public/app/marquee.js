const marquee = (selector, speed) => {
  const parentSelector = document.querySelectorAll(selector);

  parentSelector.forEach((selector) => {
    let i = 0
    window.setInterval(() => {
      if (i > window.innerWidth) { i = -window.innerWidth }
      selector.style.left = `${i}px`
      i += (i < 0) ? speed * 10 : speed
    }, 10)
  })
}

htmx.on("htmx:load", () => {
  marquee('.marquee-wrapper', 1)
});

