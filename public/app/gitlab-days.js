const elem = document.querySelectorAll("#gitlab .dock-marquee a")[1]
const then = new Date("January 31, 2017")
const timer = new Date(Date.now() - then.getTime())

const returnDays = () => {
  return Math.floor((Date.now() - then.getTime()) / 86400000)
}

elem.innerText = `${returnDays()} days`
window.setInterval(() => {
  elem.innerText = `${returnDays()} days`
}, 1500)

