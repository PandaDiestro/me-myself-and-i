const aimlines = [
  document.getElementById("aimline-1"),
  document.getElementById("aimline-2")
]

const pinpoint = document.getElementById("pinpoint")

const conv = 1 / Math.sqrt(2)

document.addEventListener('mousemove', e => {
  pinpoint.setAttribute("style", `left: ${e.clientX - 30*conv - 7}px; top: ${e.clientY - 30*conv - 7}px;`)
  aimlines[0].setAttribute("style", `left: ${e.clientX - 7}px;`)
  aimlines[1].setAttribute("style", `top: ${e.clientY - 7}px;`)
})

