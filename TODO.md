# Things that I still want to add here

## Architecture

- [ X ] Go-based backend to send the docks after generating them as html templates from a docks.yaml file
- [ X ] HTMX on the client to render the generated html docks

## Docks

- [  ] Friends' webring graph-based map
- [  ] Gentoo container instace in WASM
- [  ] Personal blog
- [  ] Academic blog
- [  ] OpenRC-based API sentinel dock





