package dock

import (
	"html/template"
	"io"
)


type templateKind int
const (
    Image       templateKind = iota
    Html
)

var typeToTemplateMap map[templateKind]string = map[templateKind]string {
    Image: `
<div id="{{.Id}}" class="dock">
  <div>
{{if .Hyperlink}}
    <a href="{{.Hyperlink}}" target="_blank">
{{else}}
    <a>
{{end}}
      <img class="dock-image transparent" src="{{.Content}}"/>
    </a>
  </div>
  <div class="dock-marquee">
    <div class="marquee-wrapper">
{{range .Marquee}}
{{if .Link}}
      <a href="{{.Link}}" target="_blank">{{.Label}}</a>
{{else}}
      <a>{{.Label}}</a>
{{end}}
{{end}}
    </div>
  </div>
</div>
{{if .Script}}
<script src="{{.Script}}"></script>
{{end}}`,
}

func newDockTemplate(contentType templateKind) (*template.Template, error) {
    newTempl, templParseErr := template.New("toml-to-dock").Parse(typeToTemplateMap[contentType])
    if templParseErr != nil {
        return &template.Template{}, templParseErr
    }

    return newTempl, nil
}

func Transform(dest io.Writer, dockStruct *DockTree) error {
    templ, templGenErr := newDockTemplate(templateKind(dockStruct.Kind))
    if templGenErr != nil {
        return templGenErr
    }

    templ.Execute(dest, dockStruct)
    return nil
}




