package dock

import (
	"io/fs"
	"os"
	"path/filepath"

	"github.com/pelletier/go-toml"
)

type HTMLElem struct {
    Tag         string
    Label       string
    Link        string
    HasLink     bool
}

type DockTree struct {
    Id          string
    Kind        int
    Content     string
    Hyperlink   string
    Marquee     []HTMLElem
    Script      string
}

func GetAllFrom(path string) ([]DockTree, error) {
    var tomlList []DockTree

    iterator := func (path string, d fs.DirEntry, err error) error {
        var tempDock DockTree

        if err != nil {
            return err
        }

        if d.IsDir() {
            return nil
        }

        if filepath.Ext(path) != ".toml" {
            return nil
        }

        pathFile, pathFileErr := os.Open(path)
        if pathFileErr != nil {
            return pathFileErr
        }

        tomlDecodeErr := toml.NewDecoder(pathFile).Decode(&tempDock)
        if tomlDecodeErr != nil {
            return tomlDecodeErr
        }

        tomlList = append(tomlList, tempDock)
        return nil
    }

    walkingErr := filepath.WalkDir(path, iterator)
    if walkingErr != nil {
        return tomlList, walkingErr
    }

    return tomlList, nil
}






