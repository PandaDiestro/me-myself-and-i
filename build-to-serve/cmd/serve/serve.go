package main

import "net/http"

const defaultDockFilePath string = "docks.html"

func docksHandler(w http.ResponseWriter, r *http.Request) {
    if r.URL.Path == "/docks" {
        http.ServeFile(w, r, defaultDockFilePath)
        return
    }

    fs := http.FileServer(http.Dir("../static"))
    http.StripPrefix("/", fs).ServeHTTP(w, r)
    return
}

func main() {
    http.HandleFunc("/", docksHandler)
    http.ListenAndServe(":8080", nil)
}




