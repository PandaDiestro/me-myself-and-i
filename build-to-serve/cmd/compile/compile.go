package main

import (
	"log"
	"os"
    "build-to-serve/internal/dock"
)

const defaultDockFilePath string = "docks.html"

func main() {
    allDocks, allDocksErr := dock.GetAllFrom(".")
    if allDocksErr != nil {
        log.Panicln(allDocksErr)
    }

    docksFile, docksFileOpenErr := os.Create(defaultDockFilePath)
    if docksFileOpenErr != nil {
        log.Panicln(docksFileOpenErr)
    }

    for i := range allDocks {
        dock.Transform(docksFile, &allDocks[i])
    }

    docksFile.Close()
}






